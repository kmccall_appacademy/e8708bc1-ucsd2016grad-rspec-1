def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, int = 2)
  res = ""
  i = 0
  while i < int
    res << str
    res << " " if i < int - 1
    i += 1
  end
  res
end

def start_of_word(str, num)
  str[0...num]
end

def first_word(sent)
  sent.split[0]
end

def titleize(title)
  words = title.split
  words.each_with_index do |word, i|
    word.capitalize!
    if i > 0 && check_little_word(word)
      word.downcase!
    end
  end
  words.join(" ")
end

def check_little_word(word)
  ("The Over And").include?(word)
end
