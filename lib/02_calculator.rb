# you will build a simple calculator script with the following methods:
#
# `add` takes two parameters and adds them
#
# `subtract` takes two parameters and subtracts the second from the first
#
# `sum` takes an *array* of parameters and adds them all together
#

def add(par1, par2)
  par1 + par2
end

def subtract(par1, par2)
  par1 - par2
end

def sum(array)
  array.reduce(0) do |acc, el|
    acc + el
  end
end

def multiply(par1, par2)
  par1 * par2
end

def power(par1, par2)
  par1 ** par2
end

def factorial(num)
  prod = 1
  (1..num).to_a.each {|el| prod *= el}
  prod
end
