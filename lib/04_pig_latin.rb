def translate(sentence)
  sentence.split.map {|word|
    latinize(word)
  }.join(" ")
end

def latinize(word)
  ind = 0
  word.chars.each_with_index do |ch, i|
    if ("aeiou").include?(ch) && word[i - 1] != "q"
      ind = i
      break
    end
  end
  word.chars.rotate(ind).join + "ay"
end
